# alpine-dotnet-5-aspnetcore
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-dotnet-5-aspnetcore)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-dotnet-5-aspnetcore)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-dotnet-5-aspnetcore/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-dotnet-5-aspnetcore/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [.NET](https://dotnet.microsoft.com/download/dotnet/5.0)
    - .NET Core is a free and open-source managed computer software framework for the Windows, Linux, and macOS operating systems.



----------------------------------------
#### Run

```sh
docker run -i -t --rm \
           forumi0721/alpine-dotnet-5-aspnetcore:[ARCH_TAG]
```



----------------------------------------
#### Usage

* Runtime



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

